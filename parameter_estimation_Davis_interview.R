library(parallel)

Mitscherlich_fun = function(PAR,A_max,A_qe,L_cp){
	dim_A = dim(PAR)
	A = pmax(0,A_max*(1-exp(-A_qe*(PAR-L_cp))))
	if(!is.null(dim_A))	A = array(A,dim = dim_A)	
	return(A)
}


calc_D_SD = function(D_SD,dayl,params){	
	#takes a D_SD, and an array of dayl, and params D_LD, CSDL and CLDL and calculates the step function
	D_LD = params$D_LD
	CSDL = params$CSDL
	CLDL = params$CLDL
	phot = D_SD + (dayl - CSDL)*(D_LD - D_SD)/(CLDL-CSDL)
	phot[dayl<=CSDL] = D_SD 
	phot[dayl>=CLDL] = D_LD
	return(phot)
}

initialize_Ve = function(Grnd.Tmp,params){
	k = params$k
	T_vmin = params$T_vmin
	T_vmax = params$T_vmax
	w = params$w
	xi = params$xi
	Ve = exp(k)*(Grnd.Tmp-T_vmin)^w*(T_vmax-Grnd.Tmp)^xi
	Ve[Grnd.Tmp>T_vmax] = 0
	Ve[Grnd.Tmp<T_vmin] = 0
	return(Ve)
}

calc_vern = function(Fb,Vern_h,params){
	Vsat = params$Vsat
	Vern = Fb + Vern_h*(1-Fb)/Vsat
	Vern[Vern > 1] = 1
	dim_Vern_h = dim(Vern_h)
	if(!is.null(dim_Vern_h)) Vern = array(Vern,dim = dim_Vern_h)
	return(Vern)
}

photosynthesis = function(PAR,Grnd_Tmp,PTU_array,HrsLight_array,alpha,theta,base_Rd){
	Rd = exp(base_Rd*(14.03-34470/(8.314*(Grnd_Tmp + 274.15))))
	Rd = Rd * HrsLight_array
	A = pmax(1/(2*theta)*(alpha*PAR + PTU_array - ((alpha*PAR + PTU_array)^2 - 4*theta*alpha*PAR*PTU_array)^(1/2)) - Rd,0)

	dim_A = dim(PAR)
	if(!is.null(dim_A)) A = array(A,dim = dim_A)
	return(A)
}

ptu_fun = function(t,params){
	dim_t = dim(t)
	# t = pmin(t,params$tmax)
	f_t = pmax(0,t-params$base_temp)
	if(!is.null(dim_t)) f_t = array(f_t,dim = dim_t)
	return(f_t)
}

filter_temperature = function(Pnight,Thermal_units,Hrs_light,params){
	Pday = params$Pday
	PTUs = Thermal_units * (Pnight*(1-Hrs_light) + Pday*Hrs_light)
	return(PTUs)
}

hot_temps = function(Grnd_Tmp,params){
	scaled_temp = pmin(1,pmax(0,Grnd_Tmp-params$min_hot_temp)/(params$max_hot_temp - params$min_hot_temp))
	hot_temp = 1-scaled_temp
	dim_temps = dim(Grnd_Tmp)
	if(!is.null(dim_temps)) hot_temp = array(hot_temp,dim = dim_temps)
	return(hot_temp)
}

calc_MPTU_at_bolting = function(	bolting_days,
									params,
									cum_thermal_time_by_day,
									growth_by_day,
									Vern_repression_by_day,
									Temp_repression_by_hour,
									FT_timecourse,
									A_array,
									Photoperiod_accelearation_by_day
								){

	proportion_temp         = params$proportion_temp
	GU_post_commit_pre_bolt = params$GU_post_commit_pre_bolt
	GU_lifespan             = params$GU_lifespan
	HT_effect               = params$HT_effect
	LD_effect 				= params$LD_effect

	MPTU_at_bolting = sapply(bolting_days,function(day){
		if(is.na(day)) return(NA)

		# calculate the predicted day when the flowering node transition occured. All evaluation will occur with respect to this day
		transition_day = day
		
		# GU lives for GU_lifespan degree_days
		alive_GU = 1
		Total_repression_by_GU_per_hour = Vern_repression_by_day[1:transition_day]  + HT_effect*mean(Temp_repression_by_hour[,transition_day])
		# Total_repression_by_GU_per_hour = Vern_repression_by_day[transition_day]  + HT_effect*mean(Temp_repression_by_hour[,transition_day])

		daily_signal = (params$D_SD + LD_effect*sum(FT_timecourse[,transition_day]))/Total_repression_by_GU_per_hour

		total_signal = sum(alive_GU * growth_by_day[1:transition_day] * daily_signal)

		return(total_signal)
	})
	return(MPTU_at_bolting * params$relative_threshold)
}


update_params_by_genotype = function(genotype,new_params,params){
	with(as.list(new_params),{
		params$Pnight = Pnight
		params$T_vmax = T_vmax
		params$Vsat = Vsat
		params$min_hot_temp = min_hot_temp
		params$max_hot_temp = max_hot_temp
		params$A_power = A_power
		params$LD_effect = 1

		# params$HT_effect = exp(HT_effect-5)
		params$HT_effect = pmax(HT_effect + HT_effect_genotype,0)
		params$GU_post_commit_pre_bolt = 10*exp(GU_post_commit_pre_bolt - 6)
		params$GU_lifespan = params$GU_post_commit_pre_bolt+10*exp(GU_lifespan - 3)


		SD_gi = SD_Col

		if(genotype == 'flm')			params$HT_effect = HT_effect_flm
		if(genotype == 'flm FRI')		params$HT_effect = HT_effect_flm		
		if(genotype == 'Kas-1')			params$HT_effect = HT_effect_Kas

 # recover()

		FLC = pmax(FLC+FLC_genotype,0)				# for RIL genotype
		if(genotype == 'Col') 			FLC = FLC_Col
		if(genotype == 'CS3879') 		FLC = FLC_Col
		if(genotype == 'Col FRI') 		FLC = FLC_Col + FLC_FRI
		if(genotype == 'hua2-3') 		FLC = FLC_hua2_3
		if(genotype == 'hua2-3 FRI') 	FLC = FLC_hua2_3_FRI		#should this be a different parameter (weaker)
		if(genotype == 'frl1-3 FRI') 	FLC = FLC_Col
		if(genotype == 'flm') 			FLC = FLC_Col
		if(genotype == 'flm FRI') 		FLC = FLC_Col + FLC_FRI
		if(genotype == 'maf2') 			FLC = FLC_Col
		if(genotype == 'maf2 FRI')	 	FLC = FLC_Col + FLC_FRI
		if(genotype == 'vin3-4 FRI') 	FLC = FLC_Col + FLC_FRI
		if(genotype == 'vin 3-1') 		FLC = FLC_Col + FLC_FRI
		if(genotype == 'Kas-1') 		FLC = FLC_Kas

		if(genotype == 'gi-2') 			FLC = FLC_Col
		if(genotype == 'fve-3')			FLC = FLC_Col + FLC_fve
		if(genotype == 'flc-3 FRI') 	FLC = FLC_Col
		params$Fb = 1/(1+pmax(0,FLC))

		SD_repression = pmax(SD+SD_genotype,0)		# for RIL genotype
		if(genotype == 'Col') 			SD_repression = SD_Col
		if(genotype == 'CS3879') 		SD_repression = SD_Col
		if(genotype == 'Col FRI') 		SD_repression = SD_Col
		if(genotype == 'hua2-3') 		SD_repression = SD_Col + SD_hua2_3
		if(genotype == 'hua2-3 FRI') 	SD_repression = SD_Col + SD_hua2_3
		if(genotype == 'frl1-3 FRI') 	SD_repression = SD_Col
		if(genotype == 'flm') 			SD_repression = SD_Col + SD_flm
		if(genotype == 'flm FRI') 		SD_repression = SD_Col + SD_flm
		if(genotype == 'maf2') 			SD_repression = SD_Col + SD_maf2
		if(genotype == 'maf2 FRI')	 	SD_repression = SD_Col + SD_maf2
		if(genotype == 'vin3-4 FRI') 	SD_repression = SD_Col
		if(genotype == 'vin 3-1') 		SD_repression = SD_Col
		if(genotype == 'Kas-1') 		SD_repression = SD_Col + SD_Kas

		if(genotype == 'gi-2') 			SD_repression = SD_gi
		if(genotype == 'fve-3')			SD_repression = SD_Col
		if(genotype == 'flc-3 FRI') 	SD_repression = SD_Col

		params$D_SD = pmin(SD_repression,1)
		params$LD_effect = (1-params$D_SD)
		if(genotype == 'gi-2')			params$LD_effect = 0
		if(genotype == 'gi-2')			params$D_LD = params$D_SD


		relative_threshold = 1 + dThresh+dThresh_genotype   	# for RIL genotype
		if(genotype == 'Col') 			relative_threshold = 1
		if(genotype == 'CS3879') 		relative_threshold = 1
		if(genotype == 'Col FRI') 		relative_threshold = 1
		if(genotype == 'hua2-3') 		relative_threshold = 1 + dThresh_hua2_3
		if(genotype == 'hua2-3 FRI') 	relative_threshold = 1 + dThresh_hua2_3
		if(genotype == 'frl1-3 FRI') 	relative_threshold = 1 
 		if(genotype == 'flm') 			relative_threshold = 1 + dThresh_flm
		if(genotype == 'flm FRI') 		relative_threshold = 1 + dThresh_flm
		if(genotype == 'maf2') 			relative_threshold = 1 + dThresh_maf2
		if(genotype == 'maf2 FRI')	 	relative_threshold = 1 + dThresh_maf2
		if(genotype == 'vin3-4 FRI') 	relative_threshold = 1 
 		if(genotype == 'vin 3-1') 		relative_threshold = 1 
 		if(genotype == 'Kas-1') 		relative_threshold = 1 + dThresh_Kas

		if(genotype == 'gi-2') 			relative_threshold = 1
		if(genotype == 'fve-3')			relative_threshold = 1
		if(genotype == 'flc-3 FRI') 	relative_threshold = 1
		params$relative_threshold = relative_threshold

		if(genotype == 'vin3-4 FRI') 	params$Vsat = Vsat_vin3
		if(genotype == 'vin 3-1') 		params$Vsat = Vsat_vin3

		return(params)
	})
}


predict_MPTU_at_bolt = function(	data,
									gen_params,
									cum_thermal_time_by_day,
									growth_by_day,
									Vern_repression_by_day,
									Temp_repression_by_hour,
									FT_timecourse,
									A_array,
									Photoperiod_accelearation_by_day
								) {
	bolting_days = data$DTB
	MPTUs = calc_MPTU_at_bolting(	bolting_days,
									gen_params,
									cum_thermal_time_by_day,
									growth_by_day,
									Vern_repression_by_day,
									Temp_repression_by_hour,
									FT_timecourse,
									A_array,
									Photoperiod_accelearation_by_day
								)
	return(MPTUs)
}


predict_DTB = function(				data,
									params,
									cum_thermal_time_by_day,
									growth_by_day,
									Vern_repression_by_day,
									Temp_repression_by_hour,
									FT_timecourse,
									A_array,
									Photoperiod_accelearation_by_day
								) {
	n_days     = sum(!is.na(growth_by_day))
	PTU_thresh = params$PTU_thresh
	sd_PTUb    = params$sd_PTUb
	MPTU_curve = calc_MPTU_at_bolting(	1:n_days,
										params,
										cum_thermal_time_by_day,
										growth_by_day,
										Vern_repression_by_day,
										Temp_repression_by_hour,
										FT_timecourse,
										A_array,
										Photoperiod_accelearation_by_day
									)
	# recover()	
	# calculate the probability of bolting on each day
	log_un_norm_daily_density = dnorm((MPTU_curve-PTU_thresh)/PTU_thresh,0,sd_PTUb,log=T) + c(-Inf,0)[(growth_by_day[1:n_days] > 0) + 1]

	# normalize the probability to sum to one
	max_d = max(log_un_norm_daily_density,na.rm=T)
	norm_factor = max_d + log(sum(exp(log_un_norm_daily_density - max_d),na.rm=T))
	norm_daily_density = exp(log_un_norm_daily_density-norm_factor)
	norm_daily_density = norm_daily_density/sum(norm_daily_density,na.rm=T)

	# calculate expected value of DTB
	days = 1:n_days
	expected_DTB = sum(days * norm_daily_density,na.rm=T)
	return(expected_DTB)
}


make_bolting_predictions = function(	prediction_function,
										data,
										new_params,
										params,
										plantings,
										Grnd_Tmp,
										HrsLight_array,
										PAR,
										Dayl,
										FT_timecourse,
										cum_thermal_time_by_day = NULL,
										Temp_repression_by_hour = NULL,
										Vern_h_by_day = NULL
									){
	current_params = params
	dim_Grnd_Tmp = dim(Grnd_Tmp)

	genotypes = unique(data$Genotype)

	# calculate light intensity total over day:
	A_array = with(as.list(current_params),Mitscherlich_fun(PAR,A_max,A_qe,L_cp))
	# A_by_day = apply(A_array,c(2,3),sum)

	# calculate growth increment per day
	if('Pnight' %in% names(new_params)) current_params$Pnight = new_params['Pnight']
	Thermal_units_array = ptu_fun(Grnd_Tmp,current_params)
	PTU_array = filter_temperature(current_params$Pnight,Thermal_units_array,HrsLight_array,current_params)

	# A = with(as.list(new_params),photosynthesis(PAR,Grnd_Tmp,Thermal_units_array,HrsLight_array,alpha,theta,base_Rd))
	A = with(as.list(new_params),photosynthesis(PAR,Grnd_Tmp,PTU_array,HrsLight_array,alpha,theta,base_Rd))

	if('A_power' %in% names(new_params)) A_power = new_params['A_power']
	growth_by_day = pmax(apply(A,c(2,3),sum,na.rm=T)^(A_power),0,na.rm=T)

	# calculate vern-type repression level at the end of each day:
	if(is.null(Vern_h_by_day)){
		Vern_h_by_hour = apply(initialize_Ve(Grnd_Tmp,current_params),3,cumsum)
		Vern_h_by_day = Vern_h_by_hour[23+seq(1,dim(Vern_h_by_hour)[1],by=24),]
	}

	# calculate (or accept as passed) temperature-dependent repression for day
	if(is.null(Temp_repression_by_hour)){
		Temp_repression_by_hour = hot_temps(Grnd_Tmp,current_params)
	}

	# calculate cumulative thermal time by day
	if(is.null(cum_thermal_time_by_day)){
		cum_thermal_time_by_day = apply(growth_by_day,2,cumsum)/(24*16)
	}


	predictions_by_genotype = mclapply(genotypes, function(gen) {
		gen_params = update_params_by_genotype(gen,new_params,params)
		gen_data = data[data$Genotype == gen,]

		Photoperiod_accelearation_by_day = calc_D_SD(gen_params$D_SD,Dayl,gen_params)#*Dayl
		Vern_repression_by_day           = 1/calc_vern(gen_params$Fb,Vern_h_by_day,gen_params)
		#calculate MPTU curves for each planting
		predicted_values = lapply(1:length(plantings),function(col) {
									if(sum(gen_data$Treatment == plantings[col]) == 0) return(c())
									prediction_function(	gen_data[gen_data$Treatment == plantings[col],],
															gen_params,
															cum_thermal_time_by_day[,col],
															growth_by_day[,col],
															Vern_repression_by_day[,col],
															Temp_repression_by_hour[,,col],
															FT_timecourse[,,col],
															A_array[,,col],
															Photoperiod_accelearation_by_day[,col]
														)
									})
		names(predicted_values) = plantings

		return(predicted_values)
	},mc.cores = ncores)
	names(predictions_by_genotype) = genotypes

	data$Predicted_value = NA
	# data$Predicted_size = NA
	for(gen in genotypes){
		for(planting in plantings){
			i = data$Genotype == gen & data$Treatment == planting
			data$Predicted_value[i] = predictions_by_genotype[[gen]][[planting]]
			# try(data$Predicted_size[i] <- cum_thermal_time_by_day[data$Predicted_value[i],match(planting,plantings)],silent=T)

		}
	}
	return(data)
}


calc_penalty = function(new_params,params){
	# strong penalty for negative parameter values
	penalty = 1e6*sum(pmin(0,new_params))
	if('HT_effect' %in% names(new_params)) {
		# recover()
		penalty = penalty + 1e6*min(0,params$max_HT_effect-new_params['HT_effect'])
	}
	return(penalty)
}


obj_fun = function(	new_params,
					data,
					param_names,
					params,
					plantings,
					Grnd_Tmp,
					HrsLight_array,
					PAR,
					Dayl,
					FT_timecourse,
					cum_thermal_time_by_day = NULL,
					Temp_repression_by_hour = NULL,
					Vern_h_by_day = NULL,
					return_pred=F,
					use_params_PTU_thresh = F,
					verbose=F
				   ) {
	names(new_params) = param_names
	penalty = calc_penalty(new_params,params)
	data = make_bolting_predictions( 	predict_MPTU_at_bolt,
										data,
										new_params,
										params,
										plantings,
										Grnd_Tmp,
										HrsLight_array,
										PAR,
										Dayl,
										FT_timecourse,
										cum_thermal_time_by_day,
										Temp_repression_by_hour,
										Vern_h_by_day
									)
	data$MPTU_at_bolting = data$Predicted_value

	if(return_pred) return(data$MPTU_at_bolting)

	if(use_params_PTU_thresh) {
		mean_MPTU = params$PTU_thresh
	} else{
		mean_MPTU = mean(data$MPTU_at_bolting,na.rm=T)
	}

	std_residual = (data$MPTU_at_bolting-mean_MPTU)/mean_MPTU

	log_lik = sum(dnorm(std_residual,0,sd(std_residual,na.rm=T),log=T),na.rm=T)

	score = -log_lik - penalty
	if(verbose) {
		if(score == 0) recover()
		result = data.frame(score = score,t(new_params))
		print(result)
	}
	return(score)
}	
